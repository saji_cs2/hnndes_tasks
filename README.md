# Hnndes_tasks

To run the program in the folder Task1, install the geopoint module using npm:

npm install geopoint

Then, you can run the program from terminal using:

node task1

You can modify the array of people in the code to test different scenarios. The attribute "health" is used to indicate whether the person is well or unwell (true for well false for unwell). Real-life locations from Google Maps are used (the same graph is represented by my geopoints).

### Issue With The Tasks
- One issue with this task1 is the weak explanation of the task. You should implement a desired output and input like big problem solving sites
- You did not address Task 2 with full appropriate speech to understand what I have to do with the auther. Should I have him signUp for the app or just  a static author in the API?

### Instructions how to run the NoteApp
- Need to have nodejs and git installed on your machine 
- Clone the repository
- Open project in VS Code
- Run npm install
- Add a config folder outside the src folder
- Add dev.en file into the config folder
- Write in the env file: PORT, MONGODB_URL (uppercase both of them)
- Assign any value for the PORT you want (express port)
- Assign this value for MONGODB_URL (mongodb+srv://saji:oqJUSHPER7asHiov@hnndes-test.87cb6fh.mongodb.net/?retryWrites=true&w=majority) without the parentheses
- Now run (npm run dev) in the terminal
### API Interaction
- First, you send a request to the route localhost:PORT/author/Signup with a JSON body containing two properties (name, email).
- To add notes, send a POST request to the route localhost:PORT/notes with a JSON body containing (email, title, body).
- To get all notes, send a GET request to the route localhost:PORT/notes with a JSON body containing (email).
- To get one note, send a GET request with the note ID as a request parameter to the route localhost:PORT/notes/:id. The JSON body is empty.
- To delete a note, send a DELETE request with the note ID as a request parameter to the route localhost:PORT/notes/:id. The JSON body is empty.
- If you want, I have added pagination functionality to the get all note route. Just send two request queries (limit, skip) example: localhost:PORT/notes?limit=3&skip=0.
### Note 
- used express ,mongoDB and mongoose as my ORM 
- the Url i provieded for the DB is a production you fully test on it or add your own DB URl
- i have a Custom Error Handler that i created to throw dynamic errors for the users .
### Issue
- One issue with this task1 is the weak explanation of the task. You should implement a desired output and input like big problem solving sites
- You did not address Task 2 with full appropriate speech to understand what I have to do with the auther. Should I have him signUp for the app or just  a static author in the API?
