const express = require('express');
require('./src/db/mongoose')
const errorHandler = require('./src/controller/error_handler')
const AutherRoute = require('./src/router/Auther')
const NoteRoute = require('./src/router/Note')
const app = express();
const port = process.env.PORT;

app.use(express.json())
app.use(AutherRoute)
app.use(NoteRoute)
app.use(errorHandler)

app.listen(port,() => {
  console.log('Server is up on port ' + port);
});
