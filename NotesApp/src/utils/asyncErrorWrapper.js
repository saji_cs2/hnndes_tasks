const customError = require('./customErrors');
const CustomErrorCode = require('./CusomErrorCode');
const E = require('./StaticErrorMessage')

const asyncErrorHandler = (func) => {
  return (req, res, next) => {
    func(req, res, next).catch((err) => {
      console.log(err);
      switch (true) {
        case err.code === CustomErrorCode.DuplicatedKey:
          const DuplicatedValue = new customError(E.Already, 400);
          return next(DuplicatedValue);
        case err.name === 'ValidationError':
          const ValidationError = new customError(err.message, 400);
          return next(ValidationError);
        default:
          return next(err);
      }
    });
  };
};

module.exports = asyncErrorHandler;
