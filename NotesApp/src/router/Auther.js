const express = require('express')
const asyncErrorWrapper = require('../utils/asyncErrorWrapper')
const autherController = require('../controller/createAuther')

const router = express.Router()

router.post('/auther/Signup', asyncErrorWrapper(autherController.CreateAuther))

module.exports = router