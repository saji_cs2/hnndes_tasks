const express = require('express');
const router = express.Router()
const asyncErrorWrapper = require('../utils/asyncErrorWrapper')

const NoteController = require('../controller/NoteControlle')

router.post('/notes',asyncErrorWrapper(NoteController.addNote))
router.get('/notes', asyncErrorWrapper(NoteController.getAllNotes))
router.get('/notes/:id', asyncErrorWrapper(NoteController.getNote))
router.delete('/notes/:id',asyncErrorWrapper(NoteController.deleteNote))
module.exports = router