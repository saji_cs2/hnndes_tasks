const mongoose = require('mongoose')


const NoteSchema = new mongoose.Schema({
    title: {
        type: String,
        max: 32,
        default : " Empty Title"
    },
    body: {
        type: String,
        require: true
    },
    auther: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref:'Auther'
    }
}, {
    timestamps: true
})

const note =  mongoose.model('note', NoteSchema)

module.exports = note