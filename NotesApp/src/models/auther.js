const mongoose = require('mongoose')
const note = require('./Note')

const autherSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        max: 32
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
}, { timestamps: true })

autherSchema.virtual('notes', {
    ref: 'note',
    localField: '_id',
    foreignField: 'auther'
})
const Auther =  mongoose.model('Auther', autherSchema)
module.exports = Auther
