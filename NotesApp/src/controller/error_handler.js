
const error_handler = (error, req, res, next) => {
    console.log(error)
    error.customErrorCode = error.customErrorCode || 0
    error.statusCode = error.statusCode || 500
    error.status = error.status || "Something Went Wrong"
    return res.status(error.statusCode).send({
        statusCode: error.statusCode,
        customErrorCode : error.customErrorCode,
        errorMessage: error.message,
        RequestStatus : error.status
    })

}

module.exports = error_handler;