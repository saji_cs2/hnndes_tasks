const Auther = require('../models/auther')
const Note = require('../models/Note')
const E = require('../utils/StaticErrorMessage')
const customError = require('../utils/customErrors')
const addNote = async (req, res) => {
    
    const auther = await Auther.findOne({ email: req.body.email })
    if (!auther) {
        throw new customError(E.UserNotFound,400);
    }
    const note = new Note({
        ...req.body,
        auther : auther._id
    })

    await note.save()
    res.status(201).send({message: 'note Created'})
}
const getAllNotes = async (req, res) => {
    const auther = await Auther.findOne({ email: req.body.email })
    if (!auther) {
        throw new customError(E.UserNotFound,400)
    }
    await auther.populate({
        path: 'notes',
        options: {
            limit: parseInt(req.query.limit),
            skip: parseInt(req.query.skip)
        }
    })

    res.status(200).send({notes: auther.notes })
}
const getNote = async (req, res) => {
    console.log(req.params.id)
    const note = await Note.findById({ _id : req.params.id })
    if (!note) {
        throw new customError(E.NoteNotFuond,400)
    }
    res.status(200).send({note})
}

const deleteNote = async (req, res) => {
    const note = await Note.deleteOne({ _id: req.params.id })
    console.log(note)
    if (note.deletedCount === 0) {
        throw new customError(E.NoteNotFuond,400)
    }
    res.status(200).send()
}

module.exports = {
    addNote,
    getAllNotes,
    getNote,
    deleteNote
}