const Auther = require('../models/auther')

const CreateAuther = async (req, res) => {
    const auther = new Auther({ ...req.body })
    await auther.save()
    res.status(201).send(auther)
}

module.exports = {
    CreateAuther
}