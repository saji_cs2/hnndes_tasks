/*
To run the program , install the geopoint module using npm:

npm install geopoint

Then, you can run the program from terminal using:

node task1

You can modify the array of people in the code to test different scenarios. 
1- used the attribute health type of boolean to indicate whether the person is well or  unwell (true for well false for unwell) 
2- used a real life locations from google maps , (the same graph is represnted by my geo points)
*/
const GeoPoint = require("geopoint");
function getDistance(point1, point2) {
    const distance = point1.distanceTo(point2, true);
    // console.log(distance)
    return distance;
}
function RideHome(unwell, wellpeople) {
    if (unwell.length === 1) {
      let minDist = Infinity;
      let nearestHealthyHuman;
      let sickPerson = unwell[0];
      for (let i = 0; i < wellpeople.length; i++) {
        let healthyPerson = wellpeople[i];
        let dist = getDistance(healthyPerson.location, sickPerson.location);
        minDist = Math.min(minDist, dist);
        if (dist === minDist) {
          nearestHealthyHuman = healthyPerson;
        }
      }
      console.log(`${nearestHealthyHuman.name} should drive ${sickPerson.name} to their home ${sickPerson.home}`);
    }
    else if (unwell.length === 2) {
      let minDist1 = Infinity;
      let minDist2 = Infinity;
      let nearestHealthyHuman1;
      let nearestHealthyHuman2;
      let sickPerson1 = unwell[0];
      let sickPerson2 = unwell[1];
      for (let i = 0; i < wellpeople.length; i++) {
        let healthyPerson = wellpeople[i];
        let dist1 = getDistance(healthyPerson.location, sickPerson1.location);
        let dist2 = getDistance(healthyPerson.location, sickPerson2.location);
        minDist1 = Math.min(minDist1, dist1);
        minDist2 = Math.min(minDist2, dist2);
        if (dist1 === minDist1) {
            nearestHealthyHuman1 = healthyPerson;
        }
        if (dist2 === minDist2) {
            nearestHealthyHuman2 = healthyPerson;
        }
      }
      if (nearestHealthyHuman1.name === nearestHealthyHuman2.name) {
        console.log(`${nearestHealthyHuman1.name} should drive ${sickPerson1.name} and ${sickPerson2.name} to their homes ${sickPerson1.home} and ${sickPerson2.home}` );
      }
      else {
        console.log( `${nearestHealthyHuman1.name} should drive ${sickPerson1.name} to their home ${sickPerson1.home}`);
        console.log( `${nearestHealthyHuman2.name} should drive ${sickPerson2.name} to their home ${sickPerson2.home}`);
      }
    }
    else {
      console.log("Too many sick people. Call an ambulance ┗(｀･∀･´●)");
    }
  }
  

  const people = [
    {
      name: "person1",
      health: true,
      home: "h1",
      location: new GeoPoint(34.731114, 36.707083),
    },
    {
      name: "person2",
      health: false,
      home: "h2",
      location: new GeoPoint(35.730514, 36.707493),
    },
    {
      name: "preson3",
      health: true,
      home: "h3",
      location: new GeoPoint(36.731032, 36.708052),
    },
    {
      name: "person4",
      health: false,
      home: "h4",
      location: new GeoPoint(37.730278, 36.708632),
    },
    {
      name: "person5",
      health: false,
      home: "h5",
      location: new GeoPoint(39.730694, 36.709186),
    },
];
const unwell = people.filter((person) => person.health === false);
const wellpeople = people.filter((person) => person.health === true);
RideHome(unwell, wellpeople)
